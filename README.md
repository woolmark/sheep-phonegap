Sheep for jQuery Mobile
----------------------------------------------------------------------------
Sheep for [PhoneGap] is an implementation [sheep] for jQuery Mobile.


How to build
----------------------------------------------------------------------------
First of all, make the 'platforms' directory.
```
% git clone git@bitbucket.org:woolmark/woolmark-phonegap.git
% cd woolmark-phonegap
% mkdir platforms
```

Build for your favorite platform.
```
% phonegap build [platform]
```

For more PhoneGap CLI, visit manual site.

http://docs.phonegap.com/en/3.3.0/guide_cli_index.md.html#The%20Command-Line%20Interface


License
----------------------------------------------------------------------------
[WTFPL] 

[sheep]: https://bitbucket.org/runne/woolmark "Sheep"
[WTFPL]: http://www.wtfpl.net "WTFPL"
[PhoneGap]: http://phonegap.com/ "PhoneGap"
