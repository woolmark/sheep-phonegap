/**
 * @license WTFPL
 * @file
 */

/**
 * sheep.
 *
 * @constructor
 */
var Sheep = function() {

    /**
     * background color.
     */
    this.COLOR_BACKGROUND = "rgb(150, 255, 150)";

    /**
     * sky color.
     */
    this.COLOR_SKY = "rgb(150, 150, 255)";

    /**
     * text color.
     */
    this.COLOR_TEXT = "rgb(0, 0, 0)";

    /**
     * canvas.
     */
    this.canvas = $("#woolmark_canvas")[0];

    /**
     * running flag.
     */
    this.running = true;

    /** 
     * sheep image.
     */
    this.sheep_image = [ new Image(), new Image() ];
    
    /**
     * background image.
     */
    this.background = new Image();

    /**
     * sheep array.
     *
     * <ul>
     *   <li>0: x position.
     *   <li>1: y position.
     *   <li>2: already jumped or not, 0:not jumped, 1:jumped.
     *   <li>3: x jump position.
     * </ul>
     */
    this.sheep_pos = [];

    /**
     * add sheep flag.
     *
     * @type boolean
     */
    this.add_sheep;

    /**
     * action counter, 0 or 1.
     */
    this.action = 0;
    
    /**
     * scale.
     */
    this.scale = 1;

    /**
     * sheep number count.
     */
    this.sheep_number = 0;
    
    for(var i = 0; i < 100; i++) {
        this.sheep_pos[i] = [];
        this.sheep_pos[i][0] = 0;
        this.sheep_pos[i][1] = -1;
        this.sheep_pos[i][2] = 0;
        this.sheep_pos[i][3] = 0;
    }

};

/**
 * start animation.
 */
Sheep.prototype.start = function() {
    var self = this;

    this.running = true;

    this.background.src = "background.gif";
    this.sheep_image[0].src = "sheep00.gif";
    this.sheep_image[1].src = "sheep01.gif";

    this.canvas.addEventListener("mousedown",  function() {
        self.onMouseDown();
    }, false);
    this.canvas.addEventListener("mouseup",  function() {
        self.onMouseUp();
    }, false);
    this.run();

};

/**
 * mouse down callback.
 */
Sheep.prototype.onMouseDown = function() {
    this.add_sheep = true;
};

/**
 * mouse up callback.
 */
Sheep.prototype.onMouseUp = function() {
    this.add_sheep = false;
};

/**
 * stop animation.
 */
Sheep.prototype.stop = function() {
    this.running = false;
};

/**
 * run as infinite loop.
 */
Sheep.prototype.run = function() {
    var self = this;

    if(!this.canvas || !this.canvas.getContext) {
        return;
    }

    setTimeout(function() {
        self.calc();
        self.draw();

        if(self.running) {
            self.run();
        }
    }, 100);
};

/**
 * calculate the sheep position and so on.
 */
Sheep.prototype.calc = function() {

    if(this.add_sheep) {
        this.addSheep();
    }

    for(var i = 0; i < this.sheep_pos.length; i++) {
        if(this.sheep_pos[i][1] < 0) {
            continue;
        }
        
        this.sheep_pos[i][0] -= 5 * this.scale;

        // sheep jumps
        if(this.sheep_pos[i][0] > this.sheep_pos[i][3] &&
        this.sheep_pos[i][0] < this.sheep_pos[i][3] + this.sheep_image[0].width * this.scale * 1.5) {
            this.sheep_pos[i][1] += 3 * this.scale;
        }

        else if(this.sheep_pos[i][0] < this.sheep_pos[i][3] &&
        this.sheep_pos[i][0] > this.sheep_pos[i][3] - this.sheep_image[0].width * this.scale * 1.5) {
            this.sheep_pos[i][1] -= 3 * this.scale;

            if(this.sheep_pos[i][2] === 0) {
                this.sheep_number++;
                this.sheep_pos[i][2] = 1;
            }
        }

        // remove a frameouted sheep
        if(this.sheep_pos[i][0] < -1 * this.sheep_image[0].width * this.scale) {
            if(i === 0) {
                this.addSheep(i);
            } else {
                this.sheep_pos[i][0] = 0;
                this.sheep_pos[i][1] = -1;
                this.sheep_pos[i][2] = 0;
                this.sheep_pos[i][3] = 0;
            }
        }

    }

    this.action = 1 - this.action;
};

/** 
 * draw the sheep and the background images.
 */
Sheep.prototype.draw = function() {
    var l,
        ctx = this.canvas.getContext("2d"),
        canvas_width = $("#woolmark_canvas").width(),
        canvas_height = $("#woolmark_canvas").height();

    ctx.beginPath();
    ctx.fillStyle = this.COLOR_BACKGROUND;
    ctx.fillRect(0, 0, canvas_width, canvas_height);

    ctx.beginPath();
    ctx.fillStyle = this.COLOR_SKY;
    ctx.fillRect(0, 0, canvas_width,
        canvas_height - (this.background.height - 10) * this.scale);

    ctx.drawImage(this.background,
        (canvas_width - this.background.width * this.scale) / 2,
        canvas_height - this.background.height * this.scale,
        this.background.width * this.scale,
        this.background.height * this.scale);

    for(l = 0; l < this.sheep_pos.length; l++) {
        if(this.sheep_pos[l][1] < 0) {
            continue;
        }
        ctx.drawImage(this.sheep_image[this.action],
            this.sheep_pos[l][0],
            canvas_height - this.sheep_pos[l][1],
            this.sheep_image[0].width * this.scale,
            this.sheep_image[0].height * this.scale);
    }
    
    ctx.fillStyle = this.COLOR_TEXT;
    ctx.textBaseline = "top";
    ctx.font = 8 * this.scale + "px sans-serif";
    ctx.fillText(this.sheep_number + " sheep", 5 * this.scale, 5 * this.scale);
};

/**
 * add new sheep.
 * @private
 */
Sheep.prototype.addSheep = function(idx) {
    var i,
        canvas_width = $("#woolmark_canvas").width(),
        canvas_height = $("#woolmark_canvas").height();
    
    if(idx === undefined) {
        for(i = 1; i < this.sheep_pos.length; i++) {
            if(this.sheep_pos[i][1] === -1) {
                idx = i;
                break;
            }
        }
    }
    
    if(idx === undefined) {
        return;
    }

    this.sheep_pos[idx][0] = canvas_width - this.sheep_image[0].width * this.scale;
    this.sheep_pos[idx][1] = Math.floor(Math.random() * 60 * this.scale) + this.sheep_image[0].height * this.scale;
    this.sheep_pos[idx][2] = 0;
    this.sheep_pos[idx][3] = this.getJumpX(this.sheep_pos[idx][1]);

};

/**
 * get jump position.
 * @return x position
 * @private
 */
Sheep.prototype.getJumpX = function(y) {
    return ($("#woolmark_canvas").width() - this.background.width * this.scale) / 2 +
            3 * (y - this.sheep_image[0].height * this.scale) / 4;
};

/**
 * resize callback.
 */
Sheep.prototype.resize = function() {
    var i,
        canvas_width = $("#woolmark_canvas").width();

    $("#woolmark_canvas").attr({
        height:$(window).height() -
            $("div.ui-header").outerHeight() -
            $("div.ui-footer").outerHeight() - 3, // -3 is magic code to escape vertical scroll
        width:$("body").width()});
    this.scale = $("#woolmark_canvas").width() / ($("#woolmark_canvas").width() / 3);

    this.addSheep(0);

};

$(function() {
    var sheep = new Sheep();
    sheep.resize();
    sheep.start();

    $(window).resize(function() {
        sheep.resize();
    });
});

